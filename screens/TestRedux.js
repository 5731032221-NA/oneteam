import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView
} from "react-native";
import TopBar from "./Shared/TopBar";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";



class TestRedux extends React.Component {
    constructor(props) {
        super(props);
        this.state = { Name: this.props.persona.name ,
                     id: this.props.persona.id};

        // let item = this.props.items.reduce((total, value) => {
        //     if (value.item_code == data) {
        //         total = value;
        //     }
        //     return total;
        // }, []);
    }

    render() {
        //const Person = this.props.Person;
        return (
        <View>
        <TextInput
            style={{height: 40, borderColor: 'gray', borderWidth: 1,marginTop: 30}}
            onChangeText={(Name) => this.setState({Name})}
            value={this.state.Name}
            
        />
        
        <Text style={{color: 'blue'}} >
        {this.state.Name}
        </Text>
        <Text style={{color: 'blue'}} >
        {this.state.id}
        </Text>
        </View>
        );
    }
}



const mapStateToProps = state => {
    return {
        persona: state.reducer.persona
        //personas: state.reducer.personas
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TestRedux);
