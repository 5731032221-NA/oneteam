'use strict';
import React, { Component, Fragment  } from 'react';
import { AppLoading, Font } from 'expo'
import {View, Image, StyleSheet, Text, StatusBar, 
        TouchableHighlight, Platform} 
        from 'react-native';
import SafeAreaView from "react-native-safe-area-view";


//import ImageFactory from 'react-native-image-picker-form'
//import { ImagePicker } from 'expo';
//import { Formik } from 'formik';
//import { Button, TextInput } from 'react-native-paper';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";
//import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'


//this.props.addName(addPerson);
// this.props.updatePersona({
//   name: persona.first_name,
//   Churn: "",
//   image: 
// });

import t from 'tcomb-form-native'; // 0.6.9

const Form = t.form.Form;
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;


const initialValues = {
  image: ''
}

var Person = t.struct({
  "Name": t.String,              // a required string
  //surname: t.maybe(t.String),  // an optional string
  //Age: t.Number,               // a required number
  "Churn": t.Boolean
  //,        // a boolean image: t.String
});

var options = {
  fields: {
    Name: {
      maxLength: 30
    }
  }
};

var value = {};
// var options = {
//   fields: {
//   image: {
//     config: {
//       title: 'Select image',
//       options: ['Open camera', 'Select from gallery', 'Cancel']
//       // Used on Android to style BottomSheet
//       // style: {
//       //   titleFontFamily: 'Roboto'
//       // }
//     },
//     error: 'No image provided',
//     factory: ImageFactory
//   }
// }};

class NameInput extends Component {

  constructor (props) {
    super(props)
    this.onPress = this.onPress.bind(this)
    value = {
      Name: this.props.persona.name,
      Churn: this.props.persona.churn
    };
    //this.onCamera = this.onCamera.bind(this)
  }

  

  // onSubmit(values) {
  //   //List of form values
  //    console.log(values);
  //   // Alert.alert(JSON.stringify(values));
  //   // Keyboard.dismiss();
  //   this.props.addName({
  //     Name: this.Person.Name,
  //     Churn: this.Person.Churn,
  //     image: values
  //   });
  //   this.props.navigation.navigate("TestRedux");
  // }

  // async _pickImage (handleChange) {
  //   let result = await ImagePicker.launchImageLibraryAsync({
  //     allowsEditing: true,
  //     aspect: [4, 3]
  //   })
  //   console.log(result)
  //   if (!result.cancelled) {
  //     handleChange(result.uri)
  //   }
  // }

  render() {
    return (
    <Fragment>
      <SafeAreaView style={{ flex: 1, backgroundColor: '#053e8b' }}>
        <View style={{
              width: "100%",
              height: STATUS_BAR_HEIGHT,
              backgroundColor: "#053e8b"
          }}>
            <StatusBar
                barStyle="light-content"
            />
        </View>
        {/* display */}  
        <View style={styles.container_top}>
          <Text style={styles.top_text}>
            Register
          </Text>
        </View> 
        <View style={styles.container}>  
          <Image style={styles.mainicon}
            source={require('../assets/logo.png')}
          />
          <Text style={styles.second_text}>
            Sign In
          </Text>
          <Text style={styles.third_text}>
            กรุณากรอกข้อมูลของท่านเพื่อเข้าใช้งาน
          </Text>
          <Text style={styles.fourth_text}>
            Username
          </Text>
          <Form
            //ref="form"
            ref={(ref) => this._formRef=ref}
            type={Person}
            value={{
              Name: this.props.persona.name,
              Churn: this.props.persona.churn
            }}
            options={options}
          />
          <Text style={styles.fifth_text}>
            Non-existent User
          </Text>
          {/* <Button
            onPress={this.onCamera}
            title="Take a profile picture"
            color="#841584"
            //accessibilityLabel="Learn more about this purple button"
          /> */}
          <TouchableHighlight style={styles.button} onPress={this.onPress} underlayColor='#99d9f4'>
            <Text style={styles.buttonText}>Sign In</Text>
          </TouchableHighlight> 
          <View style={{ height:10 }}></View>
          <View style={styles.footer} >
          
                            <View style={{flex: 1, marginLeft: 75}}  >
                            <TouchableHighlight onPress={() => this.props.navigation.navigate("NameInput")}>
                                <Image style={styles.foot_icon} 
                                source={require('../assets/shopping_active.png')}
                                />
                                </TouchableHighlight>
                            </View>
                            <View style={{flex: 1, marginLeft: 15}}  >
                            <TouchableHighlight onPress={() => this.props.navigation.navigate("ScanProduct")}>
                                <Image style={styles.foot_icon}
                                source={require('../assets/barcode.png')}
                                />
                            </TouchableHighlight>
                            </View>
                            
                            <View style={{flex: 1, marginLeft: 15}}  >
                            <TouchableHighlight onPress={() => this.props.navigation.navigate("a")}>
                                <Image style={styles.foot_icon}
                                source={require('../assets/shopping-cart.png')}
                                />
                                </TouchableHighlight>
                            </View>
                        </View>
          
          
        </View>

      </SafeAreaView>
    </Fragment>

    );
    //<TouchableHighlight style={styles.button} onPress={this.onPress} underlayColor='#99d9f4'></TouchableHighlight>
  }

  // onCamera() {
  //   // call getValue() to get the values of the form
  //     this.props.navigation.navigate("Camera");
  // }

  onPress() {
    // call getValue() to get the values of the form
    console.log("REFS.FORM");
    //console.log(this._formRef);
    var value2 = this._formRef.getValue();
    if (value2) { // if validation fails, value will be null
      // console.log(value2.Name); // value here is an instance of Person
      // console.log(value2.Churn);
      console.log(value2)
      this.props.addName({
        name: value2.Name,
        churn: value2.Churn
        // ,
        // image: ""
      });
      //this.props.navigation.navigate("ScanProduct");
      this.props.navigation.navigate("CameraProf");
    }
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#f7f7fa',
  },
  container_top: {
    height:60,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 1,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: 'grey',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 1,
    elevation: 4,
  },   
  top_text:{
    textAlign: 'center', fontSize:25, color: "#0054c6", fontFamily: "Prompt"
  },
  second_text:{
    textAlign: 'center', fontSize:36, color: "#0054c6", fontFamily: "Prompt"
  },
  third_text:{
    textAlign: 'center', fontSize:17, color: "#575757", fontFamily: "Prompt"
  },
  mainicon:{
    height: 190, width: 190, resizeMode: "contain",
  },
  fourth_text:{
    textAlign: 'center', fontSize:17, color: "#0054c6", fontFamily: "Prompt", 
    paddingTop:20, paddingBottom:10
  },
  fifth_text:{
    textAlign: 'center', fontSize:17, color: "#0054c6", fontFamily: "Prompt", 
    marginTop:-45, paddingBottom:20
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  }
  ,
  button: {
    height: 54,
    width: 295,
    backgroundColor: '#0054c6',
    borderColor: '#0054c6',
    marginTop: 0,
    borderRadius: 30,
    marginBottom: 10,
    //alignSelf: 'stretch',
    justifyContent: 'center'
  },
  content: {
    paddingTop: 10,
    padding: 16,
  },
  footer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    justifyContent: "space-between"
  },
  foot_icon: {
    width:30,
    height: 30,
    resizeMode: "contain"  
  }
});
// export default class NameInput extends Component {
//   constructor(props) {
//     super(props);
//     this.state = { text: '' };
//   }

//   render() {
//      return (
//        <TextInput
//          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
//          onChangeText={(name) => this.setState({name})}
//          value={this.state.name}
//        />
//      );
//   }
// }
///map ,

const mapStateToProps = state => {
     return {
      persona: state.reducer.persona
     };
 };

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NameInput);

// skip this line if using Create React Native App
//AppRegistry.registerComponent('AwesomeProject', () => NameInput);
