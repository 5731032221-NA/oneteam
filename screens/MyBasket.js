import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView
} from "react-native";
import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
import { LinearGradient } from "expo";
import SvgUri from "react-native-svg-uri";
import Carousel, { Pagination } from "react-native-snap-carousel";
import TopBar from "./Shared/TopBar";
import BottomBar from "./Shared/BottomBar";
import BasketItem from "./MyBasket/BasketItem";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";

const SCREEN_WIDTH = Dimensions.get("window").width;

class MyBasket extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeSlide: 0
        };
    }

    componentDidMount() {}

    render() {
        const subTotal = this.props.basket.reduce(
            (a, { fullPrice, amount }) => a + fullPrice * amount,
            0
        );
        const discount = this.props.basket.reduce(
            (a, { fullPrice, price, amount }) =>
                a + (fullPrice - price) * amount,
            0
        );
        const grandTotal = this.props.basket.reduce(
            (a, { price, amount }) => a + price * amount,
            0
        );

        return (
            <LinearGradient
                style={{
                    flex: 1
                }}
                colors={["#000000", "#434343"]}
                start={[1, 0.5]}
                end={[0, 0.5]}
            >
                <TopBar title="My Basket" />
                {this.props.basket.length > 0 && (
                    <React.Fragment>
                        <ScrollView
                            style={{
                                flex: 1,
                                zIndex: 1
                            }}
                        >
                            <View
                                style={{
                                    flex: 1,
                                    alignItems: "center",
                                    zIndex: 1
                                }}
                            >
                                <View
                                    style={{
                                        height: 20
                                    }}
                                />
                                {this.props.basket.map((item, index) => {
                                    return (
                                        <BasketItem key={index} item={item} />
                                    );
                                })}
                                <View
                                    style={{
                                        height: 20
                                    }}
                                />
                                <View
                                    style={{
                                        flexDirection: "row"
                                    }}
                                >
                                    <View style={{ flex: 1 }} />
                                    <Text
                                        style={{
                                            flex: 1,
                                            fontFamily: "Prompt",
                                            fontSize: 20,
                                            color: "#d3d3d3",
                                            textAlign: "right"
                                            // marginLeft: 50
                                        }}
                                    >
                                        Subtotal:
                                    </Text>
                                    <Text
                                        style={{
                                            flex: 1,
                                            fontFamily: "Prompt",
                                            fontSize: 20,
                                            color: "#d3d3d3",
                                            textAlign: "right",
                                            marginRight: 20
                                        }}
                                    >
                                        {subTotal}
                                    </Text>
                                </View>

                                {discount != 0 && (
                                    <View
                                        style={{
                                            flexDirection: "row"
                                        }}
                                    >
                                        <View style={{ flex: 1 }} />
                                        <Text
                                            style={{
                                                flex: 1,
                                                fontFamily: "Prompt",
                                                fontSize: 20,
                                                color: "#d3d3d3",
                                                textAlign: "right"
                                                // marginLeft: 50
                                            }}
                                        >
                                            Discount:
                                        </Text>
                                        <Text
                                            style={{
                                                flex: 1,
                                                fontFamily: "Prompt",
                                                fontSize: 20,
                                                color: "#d3d3d3",
                                                textAlign: "right",
                                                marginRight: 20
                                            }}
                                        >
                                            -{discount}
                                        </Text>
                                    </View>
                                )}
                                <View
                                    style={{
                                        flexDirection: "row",
                                        marginTop: 15
                                    }}
                                >
                                    <View style={{ flex: 1 }} />
                                    <Text
                                        style={{
                                            flex: 1,
                                            fontFamily: "Prompt_Bold",
                                            fontSize: 20,
                                            color: "#f7ce68",
                                            textAlign: "right",
                                            // marginLeft: 50
                                            textShadowColor: "#00000088",
                                            textShadowOffset: {
                                                width: 0,
                                                height: 2
                                            },
                                            textShadowRadius: 4
                                        }}
                                    >
                                        Grand Total:
                                    </Text>
                                    <Text
                                        style={{
                                            flex: 1,
                                            fontFamily: "Prompt_Bold",
                                            fontSize: 20,
                                            color: "#f7ce68",
                                            textAlign: "right",
                                            marginRight: 20
                                        }}
                                    >
                                        {grandTotal}
                                    </Text>
                                </View>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate(
                                            "MyCard"
                                        );
                                    }}
                                    style={{
                                        marginTop: 40
                                    }}
                                >
                                    <LinearGradient
                                        colors={["#c19963", "#f7ce68"]}
                                        start={[1, 0.5]}
                                        end={[0, 0.5]}
                                        style={{
                                            width: 189,
                                            height: 38,
                                            borderRadius: 100,
                                            marginBottom: 55
                                        }}
                                    >
                                        <Text
                                            style={{
                                                alignSelf: "center",
                                                fontSize: 18,
                                                fontFamily: "Prompt",
                                                marginTop: 4
                                            }}
                                        >
                                            Check Out!
                                        </Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                        <BottomBar />
                        <Image
                            style={{
                                position: "absolute",
                                left: 0,
                                right: 0,
                                bottom: 0
                            }}
                            width={SCREEN_WIDTH}
                            height={(SCREEN_WIDTH * 400) / 750}
                            source={require("../assets/Element/waveLeft.png")}
                        />
                    </React.Fragment>
                )}
                {this.props.basket.length == 0 && (
                    <React.Fragment>
                        <View style={{ alignItems: "center" }}>
                            <View style={{ height: 50 }} />
                            <SvgUri
                                width={SCREEN_WIDTH - 60}
                                height={SCREEN_WIDTH - 60}
                                source={require("../assets/Welcome/svg/welcome_EmptyBasket.svg")}
                            />
                            <Text
                                style={{
                                    fontFamily: "Prompt",
                                    fontSize: 22,
                                    letterSpacing: 0.4,
                                    textAlign: "center",
                                    color: "#ffffff"
                                }}
                            >
                                Your Basket is Empty!
                            </Text>
                        </View>
                        <View
                            style={{
                                position: "absolute",
                                width: SCREEN_WIDTH,
                                bottom: 0,
                                left: 0,
                                right: 0,
                                alignItems: "center"
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.navigate(
                                        "ScanProduct"
                                    );
                                }}
                            >
                                <LinearGradient
                                    colors={["#c19963", "#f7ce68"]}
                                    start={[1, 0.5]}
                                    end={[0, 0.5]}
                                    style={{
                                        width: 189,
                                        height: 38,
                                        borderRadius: 100,
                                        marginBottom: 55
                                    }}
                                >
                                    <Text
                                        style={{
                                            alignSelf: "center",
                                            fontSize: 18,
                                            fontFamily: "Prompt",
                                            marginTop: 4
                                        }}
                                    >
                                        Shop Now!
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </React.Fragment>
                )}
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona,
        basket: state.reducer.basket
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyBasket);
