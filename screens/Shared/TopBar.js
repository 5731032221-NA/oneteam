import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity
} from "react-native";
import SvgUri from "react-native-svg-uri";
import { withNavigation } from "react-navigation";

const SCREEN_WIDTH = Dimensions.get("window").width;

class TopBar extends React.PureComponent {
    render() {
        return (
            <View
                style={{
                    height: 82,
                    width: SCREEN_WIDTH,
                    backgroundColor: "#3c3c3c",
                    zIndex: 1
                }}
            >
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.goBack();
                    }}
                    style={{
                        position: "absolute",
                        marginTop: 20
                    }}
                >
                    <Text
                        style={{
                            fontSize: 40,
                            fontFamily: "Prompt",
                            color: "#ffffff",
                            marginLeft: 30
                        }}
                    >
                        ‹
                    </Text>
                </TouchableOpacity>
                <Text
                    style={{
                        alignSelf: "center",
                        fontSize: 22,
                        fontFamily: "Prompt",
                        color: "#ffffff",
                        marginTop: 35
                    }}
                >
                    {this.props.title}
                </Text>
            </View>
        );
    }
}

export default withNavigation(TopBar);
