import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity
} from "react-native";
import SvgUri from "react-native-svg-uri";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../../action";

const SCREEN_WIDTH = Dimensions.get("window").width;

class BasketItem extends React.PureComponent {
    render() {
        return (
            <React.Fragment>
                <View
                    style={{
                        width: SCREEN_WIDTH - 40,
                        flexDirection: "row"
                    }}
                >
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: "#fff",
                            alignItems: "center",
                            borderRadius: 4
                        }}
                    >
                        <Image
                            style={{
                                width: (SCREEN_WIDTH - 40) / 3 - 20,
                                height:
                                    (((SCREEN_WIDTH - 40) / 3 - 20) * 352) /
                                    348,
                                marginHorizontal: 10,
                                marginVertical: 10
                            }}
                            source={this.props.item.pic}
                        />
                    </View>
                    <View
                        style={{
                            flex: 2,
                            justifyContent: "space-between"
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                justifyContent: "space-between",
                                alignItems: "center"
                            }}
                        >
                            <Text
                                style={{
                                    fontFamily: "Prompt",
                                    fontSize: 18,
                                    color: "#ffffff",
                                    marginLeft: 20
                                }}
                            >
                                {this.props.item.name}
                            </Text>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.removeBasket(
                                        this.props.item.code
                                    );
                                }}
                            >
                                <Text
                                    style={{
                                        fontFamily: "Prompt",
                                        fontSize: 20,
                                        color: "#979797"
                                    }}
                                >
                                    X
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                justifyContent: "space-between",
                                alignItems: "center"
                            }}
                        >
                            <Text
                                style={{
                                    fontFamily: "Prompt",
                                    fontSize: 12,
                                    color: "#f7ce68",
                                    textAlign: "justify",
                                    marginLeft: 20
                                }}
                            >
                                {this.props.item.remark}
                            </Text>
                            <View
                                style={{
                                    flex: 1,
                                    flexDirection: "row",
                                    justifyContent: "flex-end",
                                    alignItems: "center"
                                }}
                            >
                                {this.props.item.fullPrice !=
                                    this.props.item.price && (
                                    <Text
                                        style={{
                                            fontFamily: "Prompt",
                                            fontSize: 18,
                                            color: "#ffffff",
                                            textDecorationLine: "line-through",
                                            textDecorationStyle: "solid",
                                            marginRight: 15
                                        }}
                                    >
                                        {this.props.item.fullPrice *
                                            this.props.item.amount}
                                    </Text>
                                )}
                                <Text
                                    style={{
                                        fontFamily: "Prompt",
                                        fontSize: 20,
                                        color: "#ffffff",
                                        marginRight: 15
                                    }}
                                >
                                    {this.props.item.price *
                                        this.props.item.amount}
                                </Text>
                            </View>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                justifyContent: "flex-end",
                                alignItems: "center"
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.decreaseBasket(
                                        this.props.item.code
                                    );
                                }}
                            >
                                <Image
                                    style={{
                                        width: 16,
                                        height: 16
                                    }}
                                    source={require("../../assets/Element/remove_2.png")}
                                />
                            </TouchableOpacity>
                            <Text
                                style={{
                                    width: 40,
                                    fontFamily: "Prompt",
                                    fontSize: 16,
                                    textAlign: "center",
                                    letterSpacing: 0.29,
                                    color: "#fff"
                                }}
                            >
                                {this.props.item.amount}
                            </Text>

                            <TouchableOpacity
                                onPress={() => {
                                    this.props.increaseBasket(
                                        this.props.item.code
                                    );
                                }}
                            >
                                <Image
                                    style={{
                                        width: 16,
                                        height: 16
                                    }}
                                    source={require("../../assets/Element/add_2.png")}
                                />
                            </TouchableOpacity>
                            <View style={{ width: 15 }} />
                        </View>
                    </View>
                </View>
                <View
                    style={{
                        width: SCREEN_WIDTH - 40,
                        height: 2,
                        backgroundColor: "#979797",
                        marginVertical: 10
                    }}
                />
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona,
        basket: state.reducer.basket
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BasketItem);
