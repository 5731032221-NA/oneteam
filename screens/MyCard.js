import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView
} from "react-native";
import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
import { LinearGradient } from "expo";
import SvgUri from "react-native-svg-uri";
import Carousel, { Pagination } from "react-native-snap-carousel";
import TopBar from "./Shared/TopBar";
import BottomBar from "./Shared/BottomBar";
import BasketItem from "./MyBasket/BasketItem";
import CardRow from "./MyCard/CardRow";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";

const SCREEN_WIDTH = Dimensions.get("window").width;

class MyCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeSlide: 0
        };
    }

    componentDidMount() {}

    render() {
        return (
            <LinearGradient
                style={{
                    flex: 1
                }}
                colors={["#000000", "#434343"]}
                start={[1, 0.5]}
                end={[0, 0.5]}
            >
                <TopBar title="My Card" />
                <ScrollView>
                    <View
                        style={{
                            flex: 1,
                            alignItems: "center"
                        }}
                    >
                        <View style={{ height: 10 }} />
                        <Text
                            style={{
                                alignSelf: "center",
                                fontSize: 16,
                                fontFamily: "Prompt",
                                color: "#fff"
                            }}
                        >
                            Please select your card to payment
                        </Text>
                        {this.props.persona.cards.map((item, index) => {
                            return (
                                <CardRow
                                    key={index}
                                    item={item}
                                    index={index}
                                />
                            );
                        })}
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.navigate("OrderSummary");
                            }}
                            style={{
                                marginTop: 20
                            }}
                        >
                            <LinearGradient
                                colors={["#c19963", "#f7ce68"]}
                                start={[1, 0.5]}
                                end={[0, 0.5]}
                                style={{
                                    width: 189,
                                    height: 38,
                                    borderRadius: 100
                                }}
                            >
                                <Text
                                    style={{
                                        alignSelf: "center",
                                        fontSize: 18,
                                        fontFamily: "Prompt",
                                        marginTop: 4
                                    }}
                                >
                                    Next
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                        <View style={{ height: 20 }} />
                    </View>
                </ScrollView>
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyCard);
