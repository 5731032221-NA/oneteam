import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView
} from "react-native";
import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
import { LinearGradient } from "expo";
import SvgUri from "react-native-svg-uri";
import Carousel, { Pagination } from "react-native-snap-carousel";
import TopBar from "./Shared/TopBar";
import BottomBar from "./Shared/BottomBar";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";

const SCREEN_WIDTH = Dimensions.get("window").width;

class AddToBasket extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alert: "",
            scannedItem: {
                code: "",
                pic: require("../assets/Element/close.png"),
                name: "",
                detail: "",
                remark: "",
                fullPrice: 0,
                price: 0,
                amount: 0
            }
        };
    }

    componentDidMount() {
        this.setState({
            scannedItem: this.props.navigation.getParam("scannedItem", {
                code: "",
                pic: "",
                name: "",
                detail: "",
                remark: "",
                fullPrice: 0,
                price: 0,
                amount: 0
            })
        });
    }

    render() {
        return (
            <LinearGradient
                style={{
                    flex: 1
                }}
                colors={["#000000", "#434343"]}
                start={[1, 0.5]}
                end={[0, 0.5]}
            >
                <TopBar title="Add to Basket" />
                <ScrollView
                    style={{
                        flex: 1,
                        zIndex: 1
                    }}
                >
                    <View
                        style={{
                            flex: 1,
                            zIndex: 1,
                            alignItems: "center"
                        }}
                    >
                        <View style={{ height: 30 }} />

                        <View
                            style={{
                                backgroundColor: "#fff",
                                borderTopLeftRadius: 12,
                                borderTopRightRadius: 12,
                                width: SCREEN_WIDTH - 50,
                                alignItems: "center",
                                zIndex: 1
                            }}
                        >
                            <View style={{ height: 15 }} />
                            <Image
                                style={{
                                    width: SCREEN_WIDTH - 180,
                                    height: SCREEN_WIDTH - 180
                                }}
                                source={this.state.scannedItem.pic}
                            />
                            <View style={{ height: 15 }} />
                            <View
                                style={{
                                    height: 27,
                                    width: 127,
                                    flexDirection: "row",
                                    justifyContent: "space-between",
                                    alignItems: "center"
                                }}
                            >
                                <TouchableOpacity onPress={this._handleMinus}>
                                    <Image
                                        style={{
                                            width: 27,
                                            height: 27
                                        }}
                                        source={require("../assets/Element/remove.png")}
                                    />
                                </TouchableOpacity>
                                <Text
                                    style={{
                                        fontFamily: "Prompt",
                                        fontSize: 20,
                                        textAlign: "center",
                                        letterSpacing: 0.37,
                                        color: "#000000"
                                    }}
                                >
                                    {this.state.scannedItem.amount}
                                </Text>
                                <TouchableOpacity onPress={this._handlePlus}>
                                    <Image
                                        style={{
                                            width: 27,
                                            height: 27
                                        }}
                                        source={require("../assets/Element/addd.png")}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: 37 }} />
                        </View>
                        <View
                            style={{
                                borderBottomLeftRadius: 12,
                                borderBottomRightRadius: 12,
                                backgroundColor: "#393939",
                                width: SCREEN_WIDTH - 50,
                                zIndex: 1,
                                flexDirection: "row"
                            }}
                        >
                            <View style={{ width: 16 }} />
                            <View
                                style={{
                                    flex: 1,
                                    alignItems: "center"
                                }}
                            >
                                <TouchableOpacity
                                    onPress={this._handleAdd}
                                    style={{
                                        top: -19
                                    }}
                                >
                                    <LinearGradient
                                        colors={["#c19963", "#f7ce68"]}
                                        start={[1, 0.5]}
                                        end={[0, 0.5]}
                                        style={{
                                            width: 189,
                                            height: 38,
                                            borderRadius: 100
                                        }}
                                    >
                                        <Text
                                            style={{
                                                alignSelf: "center",
                                                fontSize: 18,
                                                fontFamily: "Prompt",
                                                marginTop: 4
                                            }}
                                        >
                                            Add
                                        </Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                                {this.state.scannedItem.fullPrice !=
                                    this.state.scannedItem.price && (
                                    <View
                                        style={{
                                            flexDirection: "row",
                                            justifyContent: "space-between",
                                            alignItems: "center",
                                            alignSelf: "stretch"
                                        }}
                                    >
                                        <View>
                                            <Text
                                                style={{
                                                    fontFamily: "Prompt",
                                                    fontSize: 20,
                                                    letterSpacing: 0.24,
                                                    color: "#f7ce68"
                                                }}
                                            >
                                                {this.state.scannedItem.remark}
                                            </Text>
                                        </View>
                                        <View
                                            style={{
                                                flexDirection: "row",
                                                justifyContent: "flex-end",
                                                alignItems: "center"
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    fontFamily: "Prompt",
                                                    fontSize: 16,
                                                    textDecorationLine:
                                                        "line-through",
                                                    textDecorationStyle:
                                                        "solid",
                                                    color: "#979797"
                                                }}
                                            >
                                                {this.state.scannedItem
                                                    .fullPrice *
                                                    this.state.scannedItem
                                                        .amount}
                                            </Text>
                                            <View style={{ width: 18 }} />
                                            <Text
                                                style={{
                                                    fontFamily: "Prompt_Bold",
                                                    fontSize: 26,
                                                    color: "#f7ce68"
                                                }}
                                            >
                                                {this.state.scannedItem.price *
                                                    this.state.scannedItem
                                                        .amount}
                                                .-
                                            </Text>
                                        </View>
                                    </View>
                                )}

                                <View
                                    style={{
                                        flexDirection: "row",
                                        justifyContent: "space-between",
                                        alignItems: "center",
                                        alignSelf: "stretch"
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontFamily: "Prompt",
                                            fontSize: 20,
                                            letterSpacing: 0.24,
                                            color: "#ffffff"
                                        }}
                                    >
                                        {this.state.scannedItem.name}
                                    </Text>
                                    {this.state.scannedItem.fullPrice ==
                                        this.state.scannedItem.price && (
                                        <Text
                                            style={{
                                                fontFamily: "Prompt",
                                                fontSize: 25,
                                                color: "#f7ce68",
                                                letterSpacing: 0.46,
                                                color: "#ffffff"
                                            }}
                                        >
                                            {this.state.scannedItem.price *
                                                this.state.scannedItem.amount}
                                            .-
                                        </Text>
                                    )}
                                </View>
                                <View
                                    style={{
                                        alignSelf: "stretch"
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontFamily: "Prompt_ExtraLight",
                                            fontSize: 16,
                                            textAlign: "justify",
                                            letterSpacing: 0.13,
                                            color: "#ffffff"
                                        }}
                                    >
                                        {this.state.scannedItem.detail}
                                    </Text>
                                </View>
                                <View style={{ height: 30 }} />
                            </View>
                            <View style={{ width: 16 }} />
                        </View>
                        <View style={{ height: 30 }} />
                    </View>
                </ScrollView>
                <BottomBar />
                <Image
                    style={{
                        position: "absolute",
                        left: 0,
                        right: 0,
                        bottom: 0
                    }}
                    width={SCREEN_WIDTH}
                    height={(SCREEN_WIDTH * 400) / 750}
                    source={require("../assets/Element/waveLeft.png")}
                />
            </LinearGradient>
        );
    }

    _handleAdd = () => {
        this.props.addBasket(this.state.scannedItem);
        this.props.navigation.replace("MyBasket");
    };

    _handlePlus = () => {
        var scannedItem = {
            ...this.state.scannedItem
        };
        scannedItem.amount = this.state.scannedItem.amount + 1;
        this.setState({ scannedItem });
        // this.setState({
        //     alert: "เพิ่มสินค้านี้ได้ไม่เกิน 1 ชิ้น"
        // });
    };

    _handleMinus = () => {
        var scannedItem = {
            ...this.state.scannedItem
        };
        scannedItem.amount =
            this.state.scannedItem.amount - 1 > 0
                ? this.state.scannedItem.amount - 1
                : 1;
        this.setState({ scannedItem });
    };
}

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona,
        basket: state.reducer.basket
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddToBasket);
