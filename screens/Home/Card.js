import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity
} from "react-native";
import SvgUri from "react-native-svg-uri";

const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;

export default class Card extends React.PureComponent {
    _format(value, pattern) {
        var i = 0,
            v = value.toString();
        return pattern.replace(/#/g, _ => v[i++]);
    }

    render() {
        return (
            <ImageBackground
                style={{
                    width: SCREEN_WIDTH - 80,
                    height: ((SCREEN_WIDTH - 80) * 322) / 534,
                    justifyContent: "flex-end",
                    shadowColor: "#0000007f.8",
                    shadowOffset: {
                        width: 0,
                        height: 2
                    },
                    shadowRadius: 4,
                    shadowOpacity: 1
                }}
                source={this.props.item.png}
            >
                <Text
                    style={{
                        fontFamily: "Prompt",
                        fontSize: 20,
                        letterSpacing: 0.2,
                        textAlign: "right",
                        color: "#f7ce68",
                        position: "absolute",
                        top: 20,
                        right: 20
                    }}
                >
                    {this.props.item.balance
                        .toFixed(2)
                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")}
                </Text>
                <Text
                    style={{
                        fontFamily: "Prompt",
                        color: "#fff",
                        marginLeft: 26
                    }}
                >
                    {this.props.item.name}
                </Text>
                <Text
                    style={{
                        fontFamily: "Prompt",
                        color: "#fff",
                        marginLeft: 24,
                        marginBottom: 20,
                        letterSpacing: 6.39
                    }}
                >
                    {this._format(this.props.item.number, "### ### ### ###")}
                </Text>
            </ImageBackground>
        );
    }
}
